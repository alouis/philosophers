/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/02 10:10:01 by alouis            #+#    #+#             */
/*   Updated: 2021/03/12 19:18:51 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_one.h"

int		check_argv(char **argv)
{
	int	i;
	int	j;

	i = 1;
	while (argv[i])
	{
		j = 0;
		while (argv[i][j])
		{
			if (argv[i][j] < 48 || argv[i][j] > 57)
				return (-1);
			j++;
		}
		i++;
	}
	return (0);
}

int		init_forks(t_data *data)
{
	int	i;

	i = 0;
	if (!(data->forks = (t_fork **)malloc(sizeof(t_fork *) * data->nbr_p)))
		return (-1);
	memset(data->forks, 0, data->nbr_p);
	while (i < data->nbr_p)
	{
		if (!(data->forks[i] = (t_fork *)malloc(sizeof(t_fork))))
			return (-1);
		if (pthread_mutex_init(&data->forks[i]->mutex, NULL))
			return (-1);
		data->forks[i]->nb = i + 1;
		i++;
	}
	return (0);
}

t_data	*init_data(char **argv)
{
	t_data *data;

	if (check_argv(argv) || !(data = (t_data *)malloc(sizeof(t_data))))
		return (NULL);
	data->count = 0;
	data->death = 0;
	data->meals = 0;
	data->threads = 1;
	data->nbr_p = ft_atoul(argv[1]);
	data->die = ft_atoul(argv[2]);
	data->eat = ft_atoul(argv[3]);
	data->sleep = ft_atoul(argv[4]);
	if (argv[5])
		data->must_eat = ft_atoul(argv[5]);
	else
		data->must_eat = -1;
	if (init_forks(data))
		return (NULL);
	if (pthread_mutex_init(&data->print_activity, NULL))
		return (NULL);
	if (pthread_mutex_init(&data->print_death, NULL))
		return (NULL);
	gettimeofday(&data->start, NULL);
	return (data);
}

int		init_count_down(t_phil *p)
{
	pthread_t	alive;

	p->data->threads++;
	if (pthread_create(&alive, NULL, &count_down, (void *)p))
		return (-1);
	if (pthread_detach(alive))
		return (-1);
	return (0);
}

t_phil	*init_phil(t_data *d)
{
	t_phil	*phil;
	int		i;

	i = 0;
	if (!(phil = (t_phil *)malloc(sizeof(t_phil) * d->nbr_p)))
		return (NULL);
	while (i < d->nbr_p)
	{
		phil[i].nb = i + 1;
		phil[i].data = d;
		phil[i].must_eat = d->must_eat;
		phil[i].nb_meals = 0;
		if (i == 0)
			phil[i].left_f = d->forks[d->nbr_p - 1];
		else
			phil[i].left_f = d->forks[i - 1];
		phil[i].right_f = d->forks[i];
		i++;
	}
	return (phil);
}
