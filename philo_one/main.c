/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/02 10:00:22 by alouis            #+#    #+#             */
/*   Updated: 2021/03/14 18:57:19 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_one.h"

void	*count_down(void *phil)
{
	t_phil			*p;
	int				meals;
	struct timeval	last_meal;

	p = (t_phil *)phil;
	meals = p->nb_meals;
	gettimeofday(&last_meal, NULL);
	usleep(p->data->die * 1000);
	pthread_mutex_lock(&p->data->print_death);
	if (meals == p->nb_meals && p->must_eat && !p->data->death)
	{
		pthread_mutex_lock(&p->data->print_activity);
		printf("at %lu #%d's last meal was %lums ago\n", \
			timestamp(p->data->start), p->nb, timestamp(last_meal));
		p->data->death = 1;
		if (pthread_mutex_unlock(&p->right_f->mutex) ||
			pthread_mutex_unlock(&p->left_f->mutex))
			printf("Error. Phil #%d holding mutex\n", p->nb);
	}
	pthread_mutex_unlock(&p->data->print_death);
	p->data->threads--;
	return (NULL);
}

void	activities(t_phil *p)
{
	if (!pthread_mutex_lock(&p->right_f->mutex) &&
		!pthread_mutex_lock(&p->left_f->mutex))
		print_activity(p, FORK);
	init_count_down(p);
	print_activity(p, EAT);
	p->nb_meals++;
	usleep(p->data->eat * 1000);
	p->data->meals++;
	p->must_eat--;
	if (pthread_mutex_unlock(&p->right_f->mutex) ||
		pthread_mutex_unlock(&p->left_f->mutex))
		printf("Error. Phil #%d holding mutex\n", p->nb);
	if (p->must_eat == 0)
		return ;
	print_activity(p, SLEEP);
	usleep(p->data->sleep * 1000);
	print_activity(p, THINK);
}

void	*routine(void *phil)
{
	t_phil	*p;

	p = (t_phil *)phil;
	init_count_down(p);
	if (p->must_eat >= 0)
	{
		while (p->must_eat > 0 && !p->data->death)
			activities(p);
	}
	else
	{
		while (!p->data->death)
			activities(p);
	}
	p->data->count++;
	if (p->data->count == p->data->nbr_p)
	{
		p->data->threads--;
		p->data->death = 1;
	}
	return (NULL);
}

int		create_thread(t_phil *p)
{
	int i;

	i = -1;
	while (++i < p->data->nbr_p)
	{
		if ((i % 2) == 0)
		{
			if (pthread_create(&p[i].thread, NULL, &routine, (void *)&p[i]) ||
				pthread_detach(p[i].thread))
				return (ft_exit(p->data, p, "Error when creating thread\n"));
		}
	}
	i = 0;
	while (++i < p->data->nbr_p)
	{
		if ((i % 2) == 1)
		{
			if (pthread_create(&p[i].thread, NULL, &routine, (void *)&p[i]) ||
				pthread_detach(p[i].thread))
				return (ft_exit(p->data, p, "Error when creating thread\n"));
		}
	}
	return (0);
}

int		main(int argc, char **argv)
{
	t_data *data;
	t_phil *phil;

	data = NULL;
	phil = NULL;
	if (argc < 5 || argc > 6 || !(data = init_data(argv)))
		return (ft_exit(data, NULL, "5 or 6 numeric arguments needed\n"));
	if (data->nbr_p == 1)
		return (ft_exit(NULL, NULL, "A philosopher can't be alone."));
	if (!(phil = init_phil(data)))
		return (ft_exit(data, NULL, "Error when initializing philosophers\n"));
	if (create_thread(phil))
		return (1);
	while (phil->data->count != phil->data->nbr_p)
		usleep(1);
	while (data->threads > 0)
		usleep(1);
	if (data->must_eat >= 0)
		printf("\nPhilosophers ate %d times each. ", data->meals / data->nbr_p);
	ft_exit(data, phil, "End of the simulation\n");
	return (0);
}
