/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/02 10:37:35 by alouis            #+#    #+#             */
/*   Updated: 2021/03/12 19:11:12 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_one.h"

void			ft_usleep(unsigned int n)
{
	struct timeval	start;
	struct timeval	step;

	gettimeofday(&start, NULL);
	while (1)
	{
		usleep(50);
		gettimeofday(&step, NULL);
		if ((size_t)(((size_t)(step.tv_sec - start.tv_sec)) * 1000000 +
		((size_t)(step.tv_usec - start.tv_usec))) > n)
			break ;
	}
}

unsigned long	timestamp(struct timeval start)
{
	unsigned long	time_taken;
	struct timeval	end;

	gettimeofday(&end, NULL);
	time_taken = (end.tv_sec * 1000000 + end.tv_usec) - \
	(start.tv_sec * 1000000 + start.tv_usec);
	return (time_taken / 1000);
}

void			print_activity(t_phil *p, int activity)
{
	if (!p->data->death)
	{
		pthread_mutex_lock(&p->data->print_activity);
		if (activity == FORK && !p->data->death)
			printf("%lu #%d has taken a fork\n", timestamp(p->data->start),\
			p->nb);
		if (activity == EAT && !p->data->death)
			printf("%lu #%d is eating\n", timestamp(p->data->start), p->nb);
		if (activity == SLEEP && !p->data->death)
			printf("%lu #%d is sleeping\n", timestamp(p->data->start), p->nb);
		if (activity == THINK && !p->data->death)
			printf("%lu #%d is thinking\n", timestamp(p->data->start), p->nb);
		pthread_mutex_unlock(&p->data->print_activity);
	}
}

unsigned long	ft_atoul(const char *str)
{
	unsigned long	result;
	int				neg;

	neg = 1;
	result = 0;
	if (str == NULL)
		return (0);
	while (*str == '\t' || *str == '\n' || *str == '\v' || *str == '\f' ||
	*str == '\r' || *str == ' ')
		str++;
	if (*str == '-' || *str == '+')
	{
		if (*str == '-')
			neg *= -1;
		str++;
	}
	while (*str >= '0' && *str <= '9')
	{
		result = result * 10 + (*str - 48);
		str++;
	}
	return (result * neg);
}
