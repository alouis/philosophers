/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo_one.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/12 14:14:47 by user42            #+#    #+#             */
/*   Updated: 2021/03/12 19:10:50 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILO_ONE_H
# define PHILO_ONE_H

# include <stdio.h>
# include <unistd.h>
# include <stdlib.h>
# include <sys/time.h>
# include <pthread.h>
# include <string.h>

# define FORK 1
# define EAT 2
# define SLEEP 3
# define THINK 4
# define COUNT_DOWN 5

typedef struct		s_fork
{
	int				nb;
	pthread_mutex_t	mutex;
}					t_fork;

typedef struct		s_data
{
	int				nbr_p;
	int				die;
	int				eat;
	int				sleep;
	int				must_eat;
	struct timeval	start;
	int				count;
	int				death;
	int				meals;
	t_fork			**forks;
	int				threads;
	pthread_mutex_t	print_activity;
	pthread_mutex_t	print_death;
}					t_data;

typedef struct		s_philospher
{
	int				nb;
	int				must_eat;
	int				nb_meals;
	t_data			*data;
	pthread_t		thread;
	pthread_t		alive;
	t_fork			*right_f;
	t_fork			*left_f;
}					t_phil;

void				*count_down(void *p);
unsigned long		timestamp(struct timeval start);
t_data				*init_data(char **argv);
t_phil				*init_phil(t_data *data);
int					init_count_down(t_phil *p);
int					ft_exit(void *data, void *phil, char *msg);
unsigned long		ft_atoul(const char *str);
void				print_activity(t_phil *p, int activity);
void				ft_usleep(unsigned int n);

#endif
