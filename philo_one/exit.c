/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/02 10:14:36 by alouis            #+#    #+#             */
/*   Updated: 2021/03/04 17:57:24 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_one.h"

int	ft_exit(void *data, void *phil, char *msg)
{
	t_data *d;

	d = (t_data *)data;
	if (d != NULL)
	{
		pthread_mutex_destroy(&d->print_activity);
		pthread_mutex_destroy(&d->print_death);
		while (d->nbr_p != 0)
		{
			d->nbr_p--;
			pthread_mutex_destroy(&d->forks[d->nbr_p]->mutex);
			free(d->forks[d->nbr_p]);
		}
		free(d->forks);
		free(d);
		d = NULL;
	}
	if (phil != NULL)
	{
		free(phil);
		phil = NULL;
	}
	if (msg)
		printf("%s\n", msg);
	return (1);
}
