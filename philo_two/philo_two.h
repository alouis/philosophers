/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo_two.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/04 18:13:09 by alouis            #+#    #+#             */
/*   Updated: 2021/03/04 18:23:18 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILO_TWO_H
# define PHILO_TWO_H

# include <stdio.h>
# include <unistd.h>
# include <stdlib.h>
# include <sys/time.h>
# include <pthread.h>
# include <string.h>
# include <fcntl.h>
# include <sys/stat.h>
# include <semaphore.h>
# include <errno.h>
# include <limits.h>

# define FORK 1
# define EAT 2
# define SLEEP 3
# define THINK 4
# define COUNT_DOWN 5

typedef struct		s_data
{
	int				nbr_p;
	int				die;
	int				eat;
	int				sleep;
	int				must_eat;
	struct timeval	start;
	int				count;
	int				death;
	int				meals;
	int				threads;
	sem_t			*forks;
	sem_t			*print_activity;
	sem_t			*print_death;
}					t_data;

typedef struct		s_philospher
{
	int				nb;
	int				must_eat;
	int				nb_meals;
	t_data			*data;
	pthread_t		thread;
	pthread_t		alive;
}					t_phil;

void				*count_down(void *p);
unsigned long		timestamp(struct timeval start);
t_data				*init_data(char **argv);
t_phil				*init_phil(t_data *data);
int					init_count_down(t_phil *p);
int					ft_exit(void *data, void *phil, char *msg);
unsigned long		ft_atoul(const char *str);
void				print_activity(t_phil *p, int activity);

#endif
