/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/04 18:31:03 by alouis            #+#    #+#             */
/*   Updated: 2021/03/04 18:31:04 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_two.h"

int		check_argv(char **argv)
{
	int	i;
	int	j;

	i = 1;
	while (argv[i])
	{
		j = 0;
		while (argv[i][j])
		{
			if (argv[i][j] < 48 || argv[i][j] > 57)
				return (-1);
			j++;
		}
		i++;
	}
	return (0);
}

sem_t	*create_semaphore(const char *name, int value)
{
	sem_t	*sem;

	sem = NULL;
	sem = sem_open(name, O_CREAT | O_EXCL, S_IRWXU, value);
	while (sem == SEM_FAILED && errno == EEXIST)
	{
		sem_unlink(name);
		errno = 0;
		sem = sem_open(name, O_CREAT | O_EXCL, S_IRWXU, value);
	}
	return (sem);
}

t_data	*init_data(char **argv)
{
	t_data	*data;

	data = NULL;
	if (check_argv(argv) || !(data = (t_data *)malloc(sizeof(t_data))))
		return (NULL);
	data->count = 0;
	data->death = 0;
	data->meals = 0;
	data->threads = 1;
	data->nbr_p = ft_atoul(argv[1]);
	data->die = ft_atoul(argv[2]);
	data->eat = ft_atoul(argv[3]);
	data->sleep = ft_atoul(argv[4]);
	if (argv[5])
		data->must_eat = ft_atoul(argv[5]);
	else
		data->must_eat = -1;
	data->forks = create_semaphore("forks", data->nbr_p);
	data->print_activity = create_semaphore("activity", 1);
	data->print_death = create_semaphore("death", 1);
	gettimeofday(&data->start, NULL);
	return (data);
}

int		init_count_down(t_phil *p)
{
	pthread_t	alive;

	p->data->threads++;
	if (pthread_create(&alive, NULL, &count_down, (void *)p))
		return (-1);
	if (pthread_detach(alive))
		return (-1);
	return (0);
}

t_phil	*init_phil(t_data *d)
{
	t_phil	*phil;
	int		i;

	i = 0;
	if (!(phil = (t_phil *)malloc(sizeof(t_phil) * d->nbr_p)))
		return (NULL);
	while (i < d->nbr_p)
	{
		phil[i].nb = i + 1;
		phil[i].data = d;
		phil[i].must_eat = d->must_eat;
		phil[i].nb_meals = 0;
		i++;
	}
	return (phil);
}
