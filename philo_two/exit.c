/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: user42 <user42@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/12 14:15:23 by user42            #+#    #+#             */
/*   Updated: 2021/03/12 14:15:50 by user42           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_two.h"

int		ft_exit(void *data, void *phil, char *msg)
{
	int		i;
	t_data	*d;

	i = 0;
	d = (t_data *)data;
	if (d != NULL)
	{
		sem_unlink("forks");
		sem_close(d->forks);
		sem_unlink("activity");
		sem_close(d->print_activity);
		sem_unlink("death");
		sem_close(d->print_death);
		free(d);
		d = NULL;
	}
	if (phil != NULL)
	{
		free(phil);
		phil = NULL;
	}
	if (msg)
		printf("%s\n", msg);
	return (1);
}
