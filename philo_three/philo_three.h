/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo_three.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/12 13:22:04 by user42            #+#    #+#             */
/*   Updated: 2021/03/14 19:20:30 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILO_THREE_H
# define PHILO_THREE_H

# include <stdio.h>
# include <unistd.h>
# include <stdlib.h>
# include <sys/time.h>
# include <pthread.h>
# include <string.h>
# include <fcntl.h>
# include <sys/stat.h>
# include <semaphore.h>
# include <errno.h>
# include <limits.h>
# include <sys/types.h>
# include <sys/wait.h>
# include <signal.h>

# define FORK 1
# define EAT 2
# define SLEEP 3
# define THINK 4
# define COUNT_DOWN 5

typedef struct		s_data
{
	int				nbr_p;
	int				die;
	int				eat;
	int				sleep;
	int				must_eat;
	struct timeval	start;
	sem_t			*forks;
	sem_t			*print_activity;
	sem_t			*print_death;
	pid_t			*pids;
}					t_data;

typedef struct		s_philospher
{
	int				nb;
	int				must_eat;
	int				nb_meals;
	int				die;
	int				threads;
	pthread_t		alive;
	t_data			*data;
}					t_phil;

void				*count_down(void *p);
unsigned long		timestamp(struct timeval start);
t_data				*init_data(char **argv);
t_phil				*init_phil(t_data *d);
int					init_count_down(t_phil *p);
int					ft_exit(t_data *data, t_phil *phil, char *msg);
unsigned long		ft_atoul(const char *str);
void				print_activity(t_data *d, t_phil *p, int activity);
void				routine(t_data *d, t_phil *p);

#endif
