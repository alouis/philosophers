/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/12 13:21:41 by user42            #+#    #+#             */
/*   Updated: 2021/03/14 19:20:01 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_three.h"

int		check_argv(char **argv)
{
	int	i;
	int	j;

	i = 1;
	while (argv[i])
	{
		j = 0;
		while (argv[i][j])
		{
			if (argv[i][j] < 48 || argv[i][j] > 57)
				return (-1);
			j++;
		}
		i++;
	}
	return (0);
}

sem_t	*create_semaphore(const char *name, int value)
{
	sem_t	*sem;

	sem = NULL;
	sem = sem_open(name, O_CREAT | O_EXCL, S_IRWXU, value);
	while (sem == SEM_FAILED && errno == EEXIST)
	{
		sem_unlink(name);
		errno = 0;
		sem = sem_open(name, O_CREAT | O_EXCL, S_IRWXU, value);
	}
	return (sem);
}

t_data	*init_data(char **argv)
{
	t_data	*data;

	data = NULL;
	if (check_argv(argv) || !(data = (t_data *)malloc(sizeof(t_data))))
		return (NULL);
	data->nbr_p = ft_atoul(argv[1]);
	data->die = ft_atoul(argv[2]);
	data->eat = ft_atoul(argv[3]);
	data->sleep = ft_atoul(argv[4]);
	if (argv[5])
		data->must_eat = ft_atoul(argv[5]);
	else
		data->must_eat = -1;
	if (!(data->pids = (pid_t *)malloc(sizeof(pid_t) * data->nbr_p)))
		return (NULL);
	data->forks = create_semaphore("forks", data->nbr_p);
	data->print_activity = create_semaphore("activity", 1);
	data->print_death = create_semaphore("death", 1);
	gettimeofday(&data->start, NULL);
	return (data);
}

int		init_count_down(t_phil *p)
{
	pthread_t	alive;

	if (pthread_create(&alive, NULL, &count_down, (void *)p))
		return (-1);
	if (pthread_detach(alive))
		return (-1);
	return (0);
}

t_phil	*init_phil(t_data *d)
{
	t_phil *new;

	if (!(new = (t_phil *)malloc(sizeof(t_phil))))
		return (NULL);
	new->data = d;
	new->must_eat = d->must_eat;
	new->nb_meals = 0;
	new->threads = 0;
	return (new);
}
