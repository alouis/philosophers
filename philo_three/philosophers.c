/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philosophers.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/12 14:04:32 by user42            #+#    #+#             */
/*   Updated: 2021/03/14 19:31:34 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_three.h"

void	*count_down(void *phil)
{
	t_phil			*p;
	int				meals;
	struct timeval	last_meal;

	p = (t_phil *)phil;
	meals = p->nb_meals;
	gettimeofday(&last_meal, NULL);
	p->threads++;
	usleep(p->data->die * 1000);
	sem_wait(p->data->print_death);
	if (meals == p->nb_meals && p->must_eat)
	{
		sem_wait(p->data->print_activity);
		printf("at %lu #%d's last meal was %lums ago\n", \
		timestamp(p->data->start), p->nb, timestamp(last_meal));
		sem_post(p->data->forks);
		sem_post(p->data->forks);
		while (p->threads != 1)
			usleep(1);
		exit(10);
	}
	p->threads--;
	sem_post(p->data->print_death);
	return (NULL);
}

void	routine(t_data *d, t_phil *p)
{
	while (1)
	{
		init_count_down(p);
		sem_wait(d->forks);
		sem_wait(d->forks);
		print_activity(d, p, FORK);
		print_activity(d, p, EAT);
		usleep(d->eat * 1000);
		p->nb_meals++;
		p->must_eat--;
		sem_post(d->forks);
		sem_post(d->forks);
		if (p->nb_meals == d->must_eat)
		{
			while (p->threads != 0)
				usleep(1);
			exit(2);
		}
		print_activity(d, p, SLEEP);
		usleep(d->sleep * 1000);
		print_activity(d, p, THINK);
	}
}
