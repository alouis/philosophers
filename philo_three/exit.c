/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: user42 <user42@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/12 13:21:50 by user42            #+#    #+#             */
/*   Updated: 2021/03/12 13:44:35 by user42           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_three.h"

int		ft_exit(t_data *d, t_phil *p, char *msg)
{
	int		i;

	i = 0;
	if (d != NULL)
	{
		sem_unlink("forks");
		sem_close(d->forks);
		sem_unlink("activity");
		sem_close(d->print_activity);
		sem_unlink("death");
		sem_close(d->print_death);
		free(d->pids);
		free(d);
		d = NULL;
	}
	if (p != NULL)
	{
		free(p);
		p = NULL;
	}
	if (msg)
		printf("%s\n", msg);
	return (1);
}
