/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/12 13:21:38 by user42            #+#    #+#             */
/*   Updated: 2021/03/14 19:12:14 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_three.h"

void	kill_processes(t_data *d)
{
	int count;

	count = 0;
	while (count < d->nbr_p)
	{
		kill(d->pids[count], SIGTERM);
		count++;
	}
}

int		check_status(t_data *d, int count, int *done)
{
	int	status;

	waitpid(d->pids[count], &status, WNOHANG);
	if (WIFEXITED(status))
	{
		if (WEXITSTATUS(status) == 10)
			return (10);
		if (WEXITSTATUS(status) == 2)
			(*done)++;
	}
	return (0);
}

int		wait_children(t_data *d)
{
	int	status;
	int	count;
	int	done;

	count = 0;
	done = 0;
	while (count < d->nbr_p)
	{
		status = check_status(d, count, &done);
		if (status == 10)
			break ;
		if (done == d->nbr_p)
			break ;
		count++;
		if (count == d->nbr_p)
			count = 0;
	}
	kill_processes(d);
	if (done == d->nbr_p)
		printf("Philosophers ate %d times each. ", d->must_eat);
	return (0);
}

int		create_processes(t_data *d, t_phil *p)
{
	pid_t	pid;
	int		count;

	count = 0;
	while (count < d->nbr_p)
	{
		p->nb = count + 1;
		if ((pid = fork()) < 0)
			return (ft_exit(d, p, "Error when process forked\n"));
		if (pid == 0)
			routine(d, p);
		else
		{
			d->pids[count] = pid;
			count++;
		}
	}
	return (wait_children(d));
}

int		main(int argc, char **argv)
{
	t_data *data;
	t_phil *phil;

	data = NULL;
	phil = NULL;
	if (argc < 5 || argc > 6 || !(data = init_data(argv)))
		return (ft_exit(data, phil, "5 or 6 numeric arguments needed\n"));
	if (data->nbr_p == 1)
		return (ft_exit(data, phil, "A philosopher can't be alone."));
	if (!(phil = init_phil(data)))
		return (ft_exit(data, phil, "Malloc error\n"));
	if (create_processes(data, phil))
		return (1);
	ft_exit(data, phil, "End of the simulation\n");
	return (0);
}
