/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: user42 <user42@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/12 13:22:15 by user42            #+#    #+#             */
/*   Updated: 2021/03/12 13:22:16 by user42           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_three.h"

unsigned long	timestamp(struct timeval start)
{
	unsigned long	time_taken;
	struct timeval	end;

	gettimeofday(&end, NULL);
	time_taken = (end.tv_sec * 1000000 + end.tv_usec) - \
	(start.tv_sec * 1000000 + start.tv_usec);
	return (time_taken / 1000);
}

void			print_activity(t_data *d, t_phil *p, int activity)
{
	sem_wait(d->print_activity);
	if (activity == FORK)
		printf("%lu #%d has taken a fork\n", timestamp(d->start), p->nb);
	if (activity == EAT)
		printf("%lu #%d is eating\n", timestamp(d->start), p->nb);
	if (activity == SLEEP)
		printf("%lu #%d is sleeping\n", timestamp(d->start), p->nb);
	if (activity == THINK)
		printf("%lu #%d is thinking\n", timestamp(d->start), p->nb);
	sem_post(d->print_activity);
}

unsigned long	ft_atoul(const char *str)
{
	unsigned long	result;
	int				neg;

	neg = 1;
	result = 0;
	if (str == NULL)
		return (0);
	while (*str == '\t' || *str == '\n' || *str == '\v' || *str == '\f' ||
	*str == '\r' || *str == ' ')
		str++;
	if (*str == '-' || *str == '+')
	{
		if (*str == '-')
			neg *= -1;
		str++;
	}
	while (*str >= '0' && *str <= '9')
	{
		result = result * 10 + (*str - 48);
		str++;
	}
	return (result * neg);
}
